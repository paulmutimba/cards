//Structs and Enums - Part 1
//Paul Mutimba 500189343

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;
enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Hearts, Spades, Diamonds, Clubs };

struct Card
{
	Rank Rank;
	Suit Suit;

};

void PrintCard(Card card) {
	if (card.Rank == 2 && card.Suit == 0) //HEARTS
	{
		cout << "The Two of Hearts.\n";
	}
	else if (card.Rank == 3 && card.Suit == 0)
	{
		cout << "The Three of Hearts.\n";
	}
	else if (card.Rank == 4 && card.Suit == 0)
	{
		cout << "The Four of Hearts.\n";
	}
	else if (card.Rank == 5 && card.Suit == 0)
	{
		cout << "The Five of Hearts.\n";
	}
	else if (card.Rank == 6 && card.Suit == 0)
	{
		cout << "The Six of Hearts.\n";
	}
	else if (card.Rank == 7 && card.Suit == 0)
	{
		cout << "The Seven of Hearts.\n";
	}
	else if (card.Rank == 8 && card.Suit == 0)
	{
		cout << "The Eight of Hearts.\n";
	}
	else if (card.Rank == 9 && card.Suit == 0)
	{
		cout << "The Nine of Hearts.\n";
	}
	else if (card.Rank == 10 && card.Suit == 0)
	{
		cout << "The Jack of Hearts.\n";
	}
	else if (card.Rank == 11 && card.Suit == 0)
	{
		cout << "The Queen of Hearts.\n";
	}
	else if (card.Rank == 12 && card.Suit == 0)
	{
		cout << "The King of Hearts.\n";
	}
	else if (card.Rank == 13 && card.Suit == 0)
	{
		cout << "The Ace of Hearts.\n";
	}

	if (card.Rank == 2 && card.Suit == 1) //SPADES
	{
		cout << "The Two of Spades.\n";
	}
	else if (card.Rank == 3 && card.Suit == 1)
	{
		cout << "The Three of Spades.\n";
	}
	else if (card.Rank == 4 && card.Suit == 1)
	{
		cout << "The Four of Spades.\n";
	}
	else if (card.Rank == 5 && card.Suit == 1)
	{
		cout << "The Five of Spades.\n";
	}
	else if (card.Rank == 6 && card.Suit == 1)
	{
		cout << "The Six of Spades.\n";
	}
	else if (card.Rank == 7 && card.Suit == 1)
	{
		cout << "The Seven of Spades.\n";
	}
	else if (card.Rank == 8 && card.Suit == 1)
	{
		cout << "The Eight of Spades.\n";
	}
	else if (card.Rank == 9 && card.Suit == 1)
	{
		cout << "The Nine of Spades.\n";
	}
	else if (card.Rank == 10 && card.Suit == 1)
	{
		cout << "The Jack of Spades.\n";
	}
	else if (card.Rank == 11 && card.Suit == 1)
	{
		cout << "The Queen of Spades.\n";
	}
	else if (card.Rank == 12 && card.Suit == 1)
	{
		cout << "The King of Spades.\n";
	}
	else if (card.Rank == 13 && card.Suit == 1)
	{
		cout << "The Ace of Spades.\n";
	}

	if (card.Rank == 2 && card.Suit == 2) //DIAMONDS
	{
		cout << "The Two of Diamonds.\n";
	}
	else if (card.Rank == 3 && card.Suit == 2)
	{
		//cout << "The Three of Diamonds.\n";
		cout << card.Rank << card.Suit;
	}
	else if (card.Rank == 4 && card.Suit == 2)
	{
		cout << "The Four of Diamonds.\n";
	}
	else if (card.Rank == 5 && card.Suit == 2)
	{
		cout << "The Five of Diamonds.\n";
	}
	else if (card.Rank == 6 && card.Suit == 2)
	{
		cout << "The Six of Diamonds.\n";
	}
	else if (card.Rank == 7 && card.Suit == 2)
	{
		cout << "The Seven of Diamonds.\n";
	}
	else if (card.Rank == 8 && card.Suit == 2)
	{
		cout << "The Eight of Diamonds.\n";
	}
	else if (card.Rank == 9 && card.Suit == 2)
	{
		cout << "The Nine of Diamonds.\n";
	}
	else if (card.Rank == 10 && card.Suit == 2)
	{
		cout << "The Jack of Diamonds.\n";
	}
	else if (card.Rank == 11 && card.Suit == 2)
	{
		cout << "The Queen of Diamonds.\n";
	}
	else if (card.Rank == 12 && card.Suit == 2)
	{
		cout << "The King of Diamonds.\n";
	}
	else if (card.Rank == 13 && card.Suit == 2)
	{
		cout << "The Ace of Diamonds.\n";
	}

	if (card.Rank == 2 && card.Suit == 3) //CLUBS
	{
		cout << "The Two of Clubs.\n";
	}
	else if (card.Rank == 3 && card.Suit == 3)
	{
		cout << "The Three of Clubs.\n";
	}
	else if (card.Rank == 4 && card.Suit == 3)
	{
		cout << "The Four of Clubs.\n";
	}
	else if (card.Rank == 5 && card.Suit == 3)
	{
		cout << "The Five of Clubs.\n";
	}
	else if (card.Rank == 6 && card.Suit == 3)
	{
		cout << "The Six of Clubs.\n";
	}
	else if (card.Rank == 7 && card.Suit == 3)
	{
		cout << "The Seven of Clubs.\n";
	}
	else if (card.Rank == 8 && card.Suit == 3)
	{
		cout << "The Eight of Clubs.\n";
	}
	else if (card.Rank == 9 && card.Suit == 3)
	{
		cout << "The Nine of Clubs.\n";
	}
	else if (card.Rank == 10 && card.Suit == 3)
	{
		cout << "The Jack of Clubs.\n";
	}
	else if (card.Rank == 11 && card.Suit == 3)
	{
		cout << "The Queen of Clubs.\n";
	}
	else if (card.Rank == 12 && card.Suit == 3)
	{
		cout << "The King of Clubs.\n";
	}
	else if (card.Rank == 13 && card.Suit == 3)
	{
		cout << "The Ace of Clubs.\n";
	}
}

Card HighCard(Card card1, Card card2) 
{
	if (card1.Suit > card2.Suit) 
	{
		return card2;
	}
	else if (card1.Suit < card2.Suit)
	{
		return card1;
	}
	else if ((card1.Suit == card2.Suit) && (card1.Rank > card2.Rank))
	{
		return card2;
	}
	else if ((card1.Suit == card2.Suit) && (card1.Rank < card2.Rank))
	{
		return card1;
	}

}

int main()
{			//Print Cards	
			Card card;
			card.Rank = Three;
			card.Suit = Diamonds;
			PrintCard(card);//prints card passed in as parameter


			//High Cards
			Card card1, card2;
			
			card1.Rank = Five;
			card1.Suit = Spades;

			card2.Rank = Eight;
			card2.Suit = Diamonds;

			HighCard(card1, card2); //pass in card1 and card2

	(void)_getch();
	return 0;
}

